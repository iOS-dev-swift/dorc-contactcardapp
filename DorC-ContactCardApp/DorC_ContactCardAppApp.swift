//
//  DorC_ContactCardAppApp.swift
//  DorC-ContactCardApp
//
//  Created by Cohen, Dor on 18/10/2020.
//

import SwiftUI

@main
struct DorC_ContactCardAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
