//
//  InfoRect.swift
//  DorC-ContactCardApp
//
//  Created by Cohen, Dor on 18/10/2020.
//

import SwiftUI

struct InfoRect: View {
    let innerText: String
    let innerImage: String
    var body: some View {
        RoundedRectangle(cornerRadius: 20)
            .fill(Color.white)
            .frame( height: 40)
            .overlay(HStack {
                Image(systemName: innerImage)
                    .resizable()
                    .foregroundColor(Color(.systemTeal))
                    .frame(width: 25, height: 25)
                    .aspectRatio(contentMode: .fit)
                Text(innerText)
            })
            .padding(.all)
    }
}

struct InfoRect_Previews: PreviewProvider {
    static var previews: some View {
        InfoRect(innerText: "Text here", innerImage: "paperplane.circle")
            .previewLayout(.sizeThatFits)
    }
}
