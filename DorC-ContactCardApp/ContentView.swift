//
//  ContentView.swift
//  DorC-ContactCardApp
//
//  Created by Cohen, Dor on 18/10/2020.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        ZStack{
            Color(.systemTeal)
                .edgesIgnoringSafeArea(.all)
            VStack {
                Image("DorProfile")
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 150, height: 150)
                    .clipShape(Circle())
                    .overlay(Circle().stroke(Color.white,lineWidth: 2))
                Text("Dor Cohen")
                    .font(Font.custom("BadScript-Regular", size: 60))
                    .fontWeight(.bold)
                    .foregroundColor(Color.white)
                Text("Software Architect")
                    .font(.title2)
                    .foregroundColor(Color.white)
                Divider()
                InfoRect(innerText: "+44(0)7533 134587", innerImage: "phone.circle")
                InfoRect(innerText: "dorc.tech@gmail.com", innerImage: "envelope.circle")
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
